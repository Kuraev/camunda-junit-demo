package ru.raiffeisen.java.meetup

import org.camunda.bpm.engine.delegate.DelegateExecution
import org.camunda.bpm.engine.delegate.JavaDelegate
import org.slf4j.Logger
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component

@Component("HelloDelegate")
class HelloDelegate : JavaDelegate {
    @Autowired
    lateinit var logger: Logger

    fun sayHello() {
        logger.info("Hello World!")
    }

    override fun execute(execution: DelegateExecution?) {
        sayHello()
    }
}