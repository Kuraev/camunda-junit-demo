package ru.raiffeisen.java.meetup

import org.camunda.bpm.engine.IdentityService
import org.camunda.bpm.engine.TaskService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import java.io.Serializable

interface AutoFlowService : Serializable {
    fun completeTask(taskId: String)
}

@Service("AutoFlowService")
class AutoFlowServiceImpl
@Autowired constructor(
    private val taskService: TaskService,
    private val identityService: IdentityService
) : AutoFlowService {
    override fun completeTask(taskId: String) {
        val currentUserId = identityService.currentAuthentication.userId
        taskService.complete(taskId, mapOf(Pair("currentUser", currentUserId)))
    }
}