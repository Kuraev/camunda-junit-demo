package ru.raiffeisen.java.meetup

import org.camunda.bpm.engine.ProcessEngineConfiguration
import org.camunda.bpm.engine.impl.cfg.ProcessEngineConfigurationImpl
import org.camunda.bpm.engine.impl.el.ExpressionManager
import org.camunda.bpm.engine.spring.ProcessEngineFactoryBean
import org.camunda.bpm.engine.spring.SpringExpressionManager
import org.camunda.bpm.extension.process_test_coverage.spring.SpringProcessWithCoverageEngineConfiguration
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.ApplicationContext
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Import
import org.springframework.jdbc.datasource.DataSourceTransactionManager
import org.springframework.jdbc.datasource.SimpleDriverDataSource
import org.springframework.transaction.PlatformTransactionManager
import java.io.IOException
import javax.sql.DataSource


@Configuration
@Import(LoggerConfiguration::class)
class InMemProcessEngineConfiguration {
    @Autowired
    lateinit var applicationContext: ApplicationContext

    @Bean
    fun dataSource(): DataSource {
        val dataSource = SimpleDriverDataSource()
        dataSource.setDriverClass(org.h2.Driver::class.java)
        dataSource.url = "jdbc:h2:mem:camunda-test;DB_CLOSE_DELAY=-1"
        dataSource.username = "sa"
        dataSource.password = ""
        return dataSource
    }

    @Bean
    fun transactionManager(): PlatformTransactionManager = DataSourceTransactionManager(dataSource())

    @Bean
    @Throws(IOException::class)
    fun processEngineConfiguration(): ProcessEngineConfigurationImpl =
        SpringProcessWithCoverageEngineConfiguration().apply {
            expressionManager = expressionManager()
            transactionManager = transactionManager()
            dataSource = dataSource()
            databaseSchemaUpdate = "true"
            history = ProcessEngineConfiguration.HISTORY_FULL
            isJobExecutorActivate = false
            init()
        }

    @Bean
    fun expressionManager(): ExpressionManager = SpringExpressionManager(applicationContext, null)

    @Bean
    fun processEngine(): ProcessEngineFactoryBean = ProcessEngineFactoryBean().apply {
        processEngineConfiguration = processEngineConfiguration()
    }
}