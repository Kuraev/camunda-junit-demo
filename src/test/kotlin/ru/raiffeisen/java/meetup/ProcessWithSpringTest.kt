package ru.raiffeisen.java.meetup

import org.camunda.bpm.engine.test.Deployment
import org.camunda.bpm.engine.test.ProcessEngineRule
import org.camunda.bpm.engine.test.assertions.ProcessEngineTests
import org.camunda.bpm.engine.test.assertions.ProcessEngineTests.*
import org.junit.Assert.assertEquals
import org.junit.Assert.assertNotNull
import org.junit.ClassRule
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.boot.test.context.SpringBootTest

@RunWith(CamundaCoverageSpringJUnit4ClassRunner::class)
@SpringBootTest(classes = [ProcessEngineRuleConfiguration::class, HelloDelegate::class])
@Deployment(resources = ["processes/example.bpmn"])
class ProcessWithSpringTest {

    @Test
    fun subtractionTest() {
        //Given
        val processInstance = rule.runtimeService.startProcessInstanceByKey(
            "ExampleProcess", "12345",
            mapOf(Pair("operation", "sub"), Pair("variableOne", 2), Pair("variableTwo", 1))
        )
        assertThat(processInstance).isWaitingAt("SubtractionTask")

        //When выполняю задачу
        execute(job())

        //Then
        assertThat(processInstance).isWaitingAt("ShowTask")

        //When
        complete(task(processInstance))

        //Then
        assertThat(processInstance).isEnded
        val result = rule.historyService
            .createHistoricVariableInstanceQuery()
            .processInstanceId(processInstance.id)
            .variableName("variableResult")
            .singleResult()
        assertNotNull(result)
        assertEquals(1L, result.value)
    }

    @Test
    fun additionTest() {
        //Given
        val processInstance = rule.runtimeService.startProcessInstanceByKey(
            "ExampleProcess", "12345",
            mapOf(Pair("operation", "add"), Pair("variableOne", 2), Pair("variableTwo", 1))
        )
        assertThat(processInstance).isWaitingAt("AdditionTask")

        //When
        execute(job())

        //Then
        assertThat(processInstance).isWaitingAt("ShowTask")

        //When
        complete(task(processInstance))

        //Then
        assertThat(processInstance).isEnded
        val result = rule.historyService
            .createHistoricVariableInstanceQuery()
            .processInstanceId(processInstance.id)
            .variableName("variableResult")
            .singleResult()
        assertNotNull(result)
        assertEquals(3L, result.value)
        assertThat(processInstance).hasPassedInOrder(
            "StartEvent", "Task_182333p", "ExclusiveGatewayStart", "AdditionTask",
            "ExclusiveGatewayEnd", "ShowTask", "EndEvent"
        )
    }

    @Test
    fun errorTest() {
        //Given
        val processInstance = rule.runtimeService.startProcessInstanceByKey(
            "ExampleProcess", "12345",
            mapOf(Pair("operation", "error"))
        )

        //Then
        assertThat(processInstance).isEnded
        val result = rule.historyService
            .createHistoricActivityInstanceQuery()
            .processInstanceId(processInstance.id)
            .activityName("Error").singleResult()
        assertNotNull(result)
    }

    @Test
    fun startOnMiddleTaskTest() {
        //Given
        val processInstance = rule.runtimeService.createProcessInstanceByKey("ExampleProcess")
            .startBeforeActivity("AdditionTask")
            .setVariables(mapOf(Pair("variableOne", 2), Pair("variableTwo", 1)))
            .execute()

        //Then
        assertThat(processInstance).isWaitingAt("AdditionTask")

        //When
        execute(ProcessEngineTests.job("AdditionTask", processInstance))
        //Then
        assertThat(processInstance).isWaitingAt("ShowTask")

        //When
        complete(task(processInstance))
        //Then
        assertThat(processInstance).isEnded
    }

    companion object {
        @Rule
        @ClassRule
        @JvmStatic
        lateinit var rule: ProcessEngineRule
    }

}