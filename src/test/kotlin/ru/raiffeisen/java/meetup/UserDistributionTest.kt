package ru.raiffeisen.java.meetup

import org.camunda.bpm.engine.IdentityService
import org.camunda.bpm.engine.ProcessEngine
import org.camunda.bpm.engine.ProcessEngines.init
import org.camunda.bpm.engine.RuntimeService
import org.camunda.bpm.engine.test.Deployment
import org.camunda.bpm.engine.test.ProcessEngineRule
import org.camunda.bpm.engine.test.assertions.ProcessEngineTests
import org.camunda.bpm.engine.test.assertions.ProcessEngineTests.job
import org.junit.ClassRule
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.junit4.SpringRunner
import kotlin.test.assertFails

@RunWith(CamundaCoverageSpringJUnit4ClassRunner::class)
@ContextConfiguration
@SpringBootTest(classes = [Application::class], webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
@Deployment(resources = ["processes/task_distribution.bpmn"])
class UserDistributionTest {
    @Autowired
    private lateinit var autoFlowService: AutoFlowService


    /**
     * Тест проверяет, для какой группы будет доступно выполнение задачи
     */
    @Test
    fun candidateGroupTest() {
        val processInstance = rule.runtimeService.startProcessInstanceByKey(
            "ProcessTaskDistribution", "1",
            mapOf(Pair("taskType", "special"))
        )
        ProcessEngineTests.assertThat(processInstance).isWaitingAt("Task_Special")
        ProcessEngineTests.assertThat(processInstance).task().hasCandidateGroup("SpecialGroup")
    }

    /**
     * Тест проверяет назначение задачи на пользователя
     */
    @Test
    fun claimTest() {
        //Given
        val processInstance = rule.runtimeService.startProcessInstanceByKey(
            "ProcessTaskDistribution", "1",
            mapOf(Pair("taskType", "special"))
        )
        ProcessEngineTests.assertThat(processInstance).isWaitingAt("Task_Special")

        //When беру задачу
        val currentTask = rule.taskService.createTaskQuery().processInstanceId(processInstance.id).list().first()
        rule.taskService.claim(currentTask.id, "user12")

        //Then задача назначена на нашего пользователя и её нельзя затребовать другому человеку
        ProcessEngineTests.assertThat(processInstance).task().isAssignedTo("user12")
        assertFails { rule.taskService.claim(currentTask.id, "user13") }
    }

    /**
     * Тест демонстрирует работу identityService в тестовом окружении.
     * Мы можем тестировать сервисы, которые работают с currentAuthentication задав аутентификацию непосредственно в тесте
     * Это позволяет создавать task flow и проверять корректность его работы
     */
    @Test
    fun autoAssignmentTest() {
        //Given в систему залогинен пользователь user12, входящий в группу SpecialGroup.
        val userID = "user12"
        rule.identityService.setAuthentication(userID, listOf("SpecialGroup"))
        val processInstance = rule.runtimeService.startProcessInstanceByKey(
            "ProcessTaskDistribution", "1",
            mapOf(Pair("taskType", "special"))
        )
        ProcessEngineTests.assertThat(processInstance).isWaitingAt("Task_Special")

        //When пользователь берёт в работу задачу и заканчивает её через наш сервис AutoFlowService
        val currentTask = rule.taskService.createTaskQuery().processInstanceId(processInstance.id).list().first()
        rule.taskService.claim(currentTask.id, userID)
        ProcessEngineTests.assertThat(processInstance).task().isAssignedTo(userID)
        autoFlowService.completeTask(currentTask.id)

        //Then процесс переходит на следующую UserTask, которая автоматиески назначается на нашего пользователя
        ProcessEngineTests.assertThat(processInstance).isWaitingAt("Task_Verify_Result")
        ProcessEngineTests.assertThat(processInstance).task().isAssignedTo(userID)
    }


    companion object {
        @Rule
        @ClassRule
        @JvmStatic
        lateinit var rule: ProcessEngineRule
    }
}