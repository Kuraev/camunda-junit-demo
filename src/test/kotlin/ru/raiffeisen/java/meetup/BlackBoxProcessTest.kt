package ru.raiffeisen.java.meetup

import org.camunda.bpm.engine.impl.cfg.ProcessEngineConfigurationImpl
import org.camunda.bpm.engine.test.Deployment
import org.camunda.bpm.engine.test.ProcessEngineRule
import org.junit.*
import org.junit.Assert.assertEquals
import org.junit.Assert.assertNotNull
import org.junit.runner.RunWith
import org.springframework.boot.test.context.SpringBootTest
import java.lang.Thread.sleep

@RunWith(CamundaCoverageSpringJUnit4ClassRunner::class)
@SpringBootTest(classes = [ProcessEngineRuleConfiguration::class, HelloDelegate::class])
@Deployment(resources = ["processes/example.bpmn"])
class BlackBoxProcessTest {


    @Test
    fun subtractionTest() {
        //Given
        val processInstance = rule.runtimeService.startProcessInstanceByKey(
            "ExampleProcess", "12345",
            mapOf(Pair("operation", "sub"), Pair("variableOne", 2), Pair("variableTwo", 1))
        )

        sleep(1000)

        val result = rule.runtimeService.createVariableInstanceQuery()
            .processInstanceIdIn(processInstance.id)
            .variableName("variableResult")
            .singleResult()
        assertNotNull(result)
        assertEquals(1L, result.value)
    }

    @Test
    fun additionTest() {
        //Given
        val processInstance = rule.runtimeService.startProcessInstanceByKey(
            "ExampleProcess", "12345",
            mapOf(Pair("operation", "add"), Pair("variableOne", 2), Pair("variableTwo", 1))
        )

        sleep(1000)

        //Then
        val result = rule.historyService
            .createHistoricVariableInstanceQuery()
            .processInstanceId(processInstance.id)
            .variableName("variableResult")
            .singleResult()
        assertNotNull(result)
        assertEquals(3L, result.value)
    }

    @Test
    fun errorTest() {
        //Given
        val processInstance = rule.runtimeService.startProcessInstanceByKey(
            "ExampleProcess", "12345",
            mapOf(Pair("operation", "error"))
        )

        while (processComplete(processInstance.id)) {
            sleep(200)
        }

        //Then
        val result = rule.historyService
            .createHistoricActivityInstanceQuery()
            .processInstanceId(processInstance.id)
            .activityName("Error").singleResult()
        assertNotNull(result)
    }

    @Test
    fun startOnMiddleTaskTest() {
        //Given
        val processInstance = rule.runtimeService.createProcessInstanceByKey("ExampleProcess")
            .startBeforeActivity("AdditionTask")
            .setVariables(mapOf(Pair("variableOne", 2), Pair("variableTwo", 1)))
            .execute()

        sleep(1000)

        //Then
        val result = rule.historyService
            .createHistoricVariableInstanceQuery()
            .processInstanceId(processInstance.id)
            .variableName("variableResult")
            .singleResult()
        assertNotNull(result)
        assertEquals(3L, result.value)
    }

    private fun processNotWaitInShowTask(processInstanceId: String): Boolean =
        rule.taskService
            .createTaskQuery().active().processInstanceId(processInstanceId)
            .taskDefinitionKey("ShowTask").list().size > 0

    private fun processComplete(processInstanceId: String): Boolean =
        rule.runtimeService.createProcessInstanceQuery()
            .active().processInstanceId(processInstanceId)
            .list().size > 0


    companion object {
        @Rule
        @ClassRule
        @JvmStatic
        lateinit var rule: ProcessEngineRule

        @BeforeClass
        @JvmStatic
        fun enableProcessEngine() {
            (rule.processEngine.processEngineConfiguration as ProcessEngineConfigurationImpl).jobExecutor.start()
        }

        @AfterClass
        @JvmStatic
        fun disableProcessEngine() {
            (rule.processEngine.processEngineConfiguration as ProcessEngineConfigurationImpl).jobExecutor.shutdown()
        }

    }

}