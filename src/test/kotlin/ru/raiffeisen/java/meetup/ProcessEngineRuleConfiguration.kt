package ru.raiffeisen.java.meetup

import org.camunda.bpm.engine.ProcessEngine
import org.camunda.bpm.engine.test.ProcessEngineRule
import org.camunda.bpm.extension.process_test_coverage.junit.rules.TestCoverageProcessEngineRuleBuilder
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Import

@Configuration
@Import(InMemProcessEngineConfiguration::class)
class ProcessEngineRuleConfiguration {
    @Autowired
    lateinit var processEngine: ProcessEngine

    @Bean
    fun processEngineRule(): ProcessEngineRule {
        return TestCoverageProcessEngineRuleBuilder.create(processEngine).build()
    }
}